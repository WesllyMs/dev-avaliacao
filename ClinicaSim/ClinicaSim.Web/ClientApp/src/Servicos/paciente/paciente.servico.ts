import { Injectable, inject, Inject } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import {Observable} from "rxjs";
import { Paciente } from "../../app/paciente/modelo/paciente";
import { Http } from "@angular/http";

@Injectable({
  providedIn: "root"
})
export class PacienteServico {

  private baseURL: string;

  //constructor(private http: HttpClient, @inject('BASE_URL') baseUrl: string) {
  //  this.baseURL = baseUrl;
  //}

  //constructor(http: Http, @Inject('BASE_URL') baseUrl: string) {
  //  this.baseURL = baseUrl;
  //}

  public cadastrarPaciente(paciente: Paciente): Observable<Paciente> {

    const headers = new HttpHeaders().set('content-type', 'application/json');

    var body = {
      nome: paciente.nome,
      sobrenome: paciente.sobreNome,
      cpf: paciente.cpf,
      email: paciente.email,
      sexo: paciente.sexo
    }
    //return this.http.post<Paciente>(this.baseURL + "api/paciente", body, { headers });

    return this.http.post<Paciente>("https://localhost:44338/api/paciente", body, { headers });
  }

}
