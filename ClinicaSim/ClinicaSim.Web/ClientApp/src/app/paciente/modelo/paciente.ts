export class Paciente {
  id: number;
  nome: string;
  sobreNome: string;
  cpf: number;
  sexo: string;
  email: string;
}
