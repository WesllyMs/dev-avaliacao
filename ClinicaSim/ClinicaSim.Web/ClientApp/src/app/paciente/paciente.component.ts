import {Component, OnInit} from "@angular/core"
import { Paciente } from "./modelo/paciente";
import { PacienteServico } from "../../Servicos/paciente/paciente.servico";
@Component({
  selector: "app-paciente",
  templateUrl: "./paciente.component.html",
  styleUrls:["./paciente.componente.css"]
})
export class PacienteComponent implements OnInit{
  public paciente: Paciente;
  
  constructor(private pacienteServico: PacienteServico) {

  }

  ngOnInit(): void {
    this.paciente = new Paciente();
  }

  public cadastrar() {
    this.pacienteServico.cadastrarPaciente(this.paciente)
      .subscribe(
        pacienteJson => { },
        e => {}
      );

  }

}
