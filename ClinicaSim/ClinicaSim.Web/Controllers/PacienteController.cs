﻿using ClinicaSim.Dominio.Contratos;
using ClinicaSim.Dominio.Entidades;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ClinicaSim.Web.Controllers
{
    [Route("api/[controller]")]
    public class PacienteController : Controller
    {
        private readonly IPacienteRepositorio _pacienteRepositorio;
        public PacienteController(IPacienteRepositorio pacienteRepositorio)
        {

            _pacienteRepositorio = pacienteRepositorio;
        }

        [HttpGet]
        public IActionResult Get()
        {
            try
            {

                return Ok(_pacienteRepositorio.ObterTodos());
            }
            catch (Exception ex)
            {
                return BadRequest(ex.ToString());
            }
        }

        [HttpPost]
        public IActionResult Post([FromBody] Paciente paciente)
        {
            try
            {
            //    var paciente = new Paciente();

            //    paciente.Nome = "";
            //    paciente.SobreNome = "";
            //    paciente.cpf = 2121;
                _pacienteRepositorio.Adcionar(paciente);
            return Created("api/paciente", paciente);

            }catch(Exception ex)
            {
                return BadRequest(ex.ToString());
            }
        }
    }
}
