﻿using ClinicaSim.Dominio.Entidades;
using System;
using System.Collections.Generic;
using System.Text;

namespace ClinicaSim.Dominio.Contratos
{
    public interface IConsultaRepositorio: IBaseRepositorio<Consulta>
    {

    }
}
