﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ClinicaSim.Dominio.Entidades
{
    public class Agenda
    {
        public int Id { get; set; }
        public DateTime Data { get; set; }
        public int MedicoId { get; set; }
        public virtual Medico Medico { get; set; }
        public int ConsultaId { get; set; }
        public virtual Consulta Consulta { get; set; }
    }
}
