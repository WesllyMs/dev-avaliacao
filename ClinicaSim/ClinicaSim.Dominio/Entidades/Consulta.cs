﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ClinicaSim.Dominio.Entidades
{
    public class Consulta
    {
        public int Id { get; set; }
        public int PacienteID { get; set; }
        public virtual Paciente Paciente { get; set; }
        public int AgendaID { get; set; }
        public virtual Agenda Agenda { get; set; }
        public Double Preco { get; set; }
        public String Diagnostico { get; set; }
        public int FormaPagamentoId { get; set; }
        public virtual FormaPagamento FormaPagamento { get; set; }

    }
}
