﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ClinicaSim.Dominio.Entidades
{
    public class Paciente
    {
        public int Id { get; set; }
        public String Nome { get; set; }
        public String SobreNome { get; set; }
        public int cpf { get; set; }
        public String Sexo { get; set; }
        public String Email { get; set; }

        public virtual ICollection<Consulta> Consultas { get; set; }
    }
}
