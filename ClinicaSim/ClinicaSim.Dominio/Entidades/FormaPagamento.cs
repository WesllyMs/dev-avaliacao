﻿using ClinicaSim.Dominio.Enumerados;
using System;
using System.Collections.Generic;
using System.Text;

namespace ClinicaSim.Dominio.Entidades
{
    public class FormaPagamento
    {
        public int Id { get; set; }
        public String Descricao { get; set; }

        public String Nome { get; set; }
        public bool EhAvista
        {
            get { return Id == (int)TipoFormaPagamentoEnum.AVista; }
        }

        public bool EhBoleto
        {
            get { return Id == (int)TipoFormaPagamentoEnum.Boleto; }
        }

        public bool EhCredito
        {
            get { return Id == (int)TipoFormaPagamentoEnum.CartaoCredito; }
        }

        public bool EhDeposito
        {
            get { return Id == (int)TipoFormaPagamentoEnum.Deposito; }
        }

        public bool EhNaoDefinido
        {
            get { return Id == (int)TipoFormaPagamentoEnum.NaoDefinido; }
        }
    }
}
