﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ClinicaSim.Dominio.Entidades
{
    public class Medico
    {

        public int Id { get; set; }
        public String Nome { get; set; }
        public String SobreNome { get; set; }
        public int cpf { get; set; }
        public String Sexo { get; set; }
        public String Email { get; set; }
        public String Registro { get; set; }

        public virtual ICollection<Agenda> Agendas { get; set; }

        public String Especialidade { get; set; }
    }
}
