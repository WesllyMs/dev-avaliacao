﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ClinicaSim.Dominio.Enumerados
{
    public enum TipoFormaPagamentoEnum
    {
        AVista = 1,
        Boleto = 2,
        CartaoCredito =3,
        Deposito = 4,
        NaoDefinido = 5  
    }
}
