﻿using ClinicaSim.Dominio.Entidades;
using ClinicaSim.Repositorio.Config;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace ClinicaSim.Repositorio.Contexto
{
    public class ClinicaSimContexto:DbContext
    {
        public DbSet<Paciente> Pacientes { get; set; }
        public DbSet<Medico> Medicos { get; set; }
        public DbSet<Consulta> Consultas { get; set; }
        public DbSet<Agenda> Agendas { get; set; }
        public DbSet<FormaPagamento> FormaPagamento { get; set; }

        public ClinicaSimContexto(DbContextOptions options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //Classes de mapeamento para a base de dados
            modelBuilder.ApplyConfiguration(new PacienteConfiguration());
            modelBuilder.ApplyConfiguration(new MedicoConfiguration());
            modelBuilder.ApplyConfiguration(new AgendaConfiguration());
            modelBuilder.ApplyConfiguration(new ConsultaConfiguration());
            modelBuilder.ApplyConfiguration(new FormaPagamentoConfiguration());

            modelBuilder.Entity<FormaPagamento>().HasData(
                new FormaPagamento() {
                    Id =1,
                    Nome ="Avista",
                    Descricao ="Forma Pagamento Avista"
                }, new FormaPagamento()
                {
                    Id = 2,
                    Nome = "Boleto",
                    Descricao = "Forma Pagamento Boleto"
                }
                , new FormaPagamento()
                {
                    Id = 3,
                    Nome = "Cartão de Credito",
                    Descricao = "Forma Pagamento Cartão de credito"
                }, new FormaPagamento()
                {
                    Id = 4,
                    Nome = "Deposito",
                    Descricao = "Forma Pagamento Boleto"
                }, new FormaPagamento()
                {
                    Id = 5,
                    Nome = "Não Definido",
                    Descricao = "Forma Pagamento Não Definida"
                }
                );
            base.OnModelCreating(modelBuilder);
        }

    }
}
