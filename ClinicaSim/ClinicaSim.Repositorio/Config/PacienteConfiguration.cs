﻿using ClinicaSim.Dominio.Entidades;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace ClinicaSim.Repositorio.Config
{
    public class PacienteConfiguration : IEntityTypeConfiguration<Paciente>
    {
        public void Configure(EntityTypeBuilder<Paciente> builder)
        {
            builder
                .HasKey(p => p.Id);

            builder
                .Property(p => p.Nome)
                .IsRequired()
                .HasMaxLength(200);

            builder
                .Property(p => p.cpf)
                .IsRequired();

            builder
                .HasMany(p => p.Consultas)
                .WithOne(c => c.Paciente);
        }
    }
}
