﻿using ClinicaSim.Dominio.Entidades;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace ClinicaSim.Repositorio.Config
{
    public class ConsultaConfiguration : IEntityTypeConfiguration<Consulta>
    {
        public void Configure(EntityTypeBuilder<Consulta> builder)
        {
            builder
                .HasKey(c => c.Id);
            builder
                .Property(c => c.PacienteID)
                .IsRequired();
            builder
                .Property(c => c.AgendaID)
                .IsRequired();
            builder
                .Property(c => c.Preco)
                .IsRequired();
            builder
                .HasOne(c => c.Agenda)
                .WithOne(c => c.Consulta)
                .HasForeignKey<Agenda>(a => a.ConsultaId);
        }
    }
}
