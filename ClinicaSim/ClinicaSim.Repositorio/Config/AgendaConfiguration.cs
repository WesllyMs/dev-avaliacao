﻿using ClinicaSim.Dominio.Entidades;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace ClinicaSim.Repositorio.Config
{
    public class AgendaConfiguration : IEntityTypeConfiguration<Agenda>
    {
        public void Configure(EntityTypeBuilder<Agenda> builder)
        {
            builder
                .HasIndex(a => a.Id);
            builder
                .Property(a => a.MedicoId)
                .IsRequired();
            builder
                .Property(a => a.Data)
                .IsRequired();
            builder
                .HasOne(a => a.Consulta)
                .WithOne(a => a.Agenda)
                .HasForeignKey<Consulta>(c => c.AgendaID);
        }
    }
}
