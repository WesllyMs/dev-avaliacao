﻿using ClinicaSim.Dominio.Entidades;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace ClinicaSim.Repositorio.Config
{
    public class MedicoConfiguration : IEntityTypeConfiguration<Medico>
    {
        public void Configure(EntityTypeBuilder<Medico> builder)
        {
            builder
                .HasKey(m => m.Id);
            builder
                .Property(m => m.cpf).IsRequired();
            builder
                .Property(m => m.Nome).IsRequired().HasMaxLength(200);

            builder
                .HasMany(m => m.Agendas)
                .WithOne(a => a.Medico);
        }
    }
}
