﻿using ClinicaSim.Dominio.Contratos;
using ClinicaSim.Dominio.Entidades;
using ClinicaSim.Repositorio.Contexto;
using System;
using System.Collections.Generic;
using System.Text;

namespace ClinicaSim.Repositorio.Reposiotorios
{
    public class ConsultaRepositorio : BaseRepositorio<Consulta>, IConsultaRepositorio
    {
        public ConsultaRepositorio(ClinicaSimContexto clinicaSimContexto) : base(clinicaSimContexto)
        {
        }
    }
}
