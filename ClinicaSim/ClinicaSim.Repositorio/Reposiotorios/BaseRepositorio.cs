﻿using ClinicaSim.Dominio.Contratos;
using ClinicaSim.Repositorio.Contexto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ClinicaSim.Repositorio.Reposiotorios
{
    public class BaseRepositorio<TEntity> : IBaseRepositorio<TEntity> where TEntity: class {
        protected readonly ClinicaSimContexto ClinicaSimContexto;

        public BaseRepositorio(ClinicaSimContexto clinicaSimContexto)
        {
            ClinicaSimContexto = clinicaSimContexto;
        }
        public void Adcionar(TEntity entity)
        {
            ClinicaSimContexto.Set<TEntity>().Add(entity);
            ClinicaSimContexto.SaveChanges();
        }

        public void Atualizar(TEntity entity)
        {
            ClinicaSimContexto.Set<TEntity>().Update(entity);
            ClinicaSimContexto.SaveChanges();
        }

        public TEntity ObterPorId(int Id)
        {
            return ClinicaSimContexto.Set<TEntity>().Find(Id);
        }

        public IEnumerable<TEntity> ObterTodos()
        {
            return ClinicaSimContexto.Set<TEntity>().ToList();
        }

        public void Remover(TEntity entity)
        {
            ClinicaSimContexto.Remove(entity);
            ClinicaSimContexto.SaveChanges();
        }


        public void Dispose()
        {
            ClinicaSimContexto.Dispose();
        }
    }

}
