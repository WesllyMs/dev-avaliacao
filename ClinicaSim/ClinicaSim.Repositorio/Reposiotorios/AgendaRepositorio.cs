﻿using ClinicaSim.Dominio.Contratos;
using ClinicaSim.Dominio.Entidades;
using ClinicaSim.Repositorio.Contexto;
using System;
using System.Collections.Generic;
using System.Text;

namespace ClinicaSim.Repositorio.Reposiotorios
{
    public class AgendaRepositorio : BaseRepositorio<Agenda>, IAgendaRepositorio
    {
        public AgendaRepositorio(ClinicaSimContexto clinicaSimContexto) : base(clinicaSimContexto)
        {
        }
    }
}
