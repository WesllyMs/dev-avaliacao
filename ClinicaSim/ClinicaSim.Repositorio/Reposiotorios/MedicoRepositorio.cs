﻿using ClinicaSim.Dominio.Contratos;
using ClinicaSim.Dominio.Entidades;
using ClinicaSim.Repositorio.Contexto;
using System;
using System.Collections.Generic;
using System.Text;

namespace ClinicaSim.Repositorio.Reposiotorios
{
    public class MedicoRepositorio : BaseRepositorio<Medico>, IMedicoRepositorio
    {
        public MedicoRepositorio(ClinicaSimContexto clinicaSimContexto) : base(clinicaSimContexto)
        {
        }
    }
}
